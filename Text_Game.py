import random

class Item:
    # Definition of Items ready to pick up 
    def __init__(self, name, description):
        self.name = name
        self.description = description

    def use(self):
        pass

class Light(Item):
    def use(self):
        print("Now, you can see much more with light going on")

    def __str__(self):
        return self.description

class Map(Item):
    def use(self):
        print("UWith the map, it's going to be much easier")

    def __str__(self):
        return self.description

class Food(Item):
    def use(self):
        print("Now,you're full")

    def __str__(self):
        return self.description   

class Room:
    # CLass for each rooms we can visit
    def __init__(self, description, options,):
        self.description = description
        self.options = options
        self.exits = {}

    def add_exit(self, direction, room):
        self.exits[direction] = room

    def get_exit(self, direction):
        return self.exits.get(direction)

    def get_exit_string(self):
        return ", ".join(list(self.exits.keys()))

class Game:
    def __init__(self):
        # Description of rooms
        self.rooms = {
        "Room 1": Room("You're in a small room. You can go only west or east", ["e","t","i","p","u", "d", "w"]),
        "Room 2": Room("You're in big room and you can choose where to go: west, north or south.", ["n", "s", "w","t","i","p","u", "d"]),
        "Room 3": Room("Now you enter into huge cave. The only way to go is north & east.", ["n", "e","t","i","p","u", "d"])
        }

        # You can connect the rooms togehter
        self.rooms["Room 1"].add_exit("w", self.rooms["Room 2"])
        self.rooms["Room 2"].add_exit("e", self.rooms["Room 1"])
        self.rooms["Room 2"].add_exit("n", self.rooms["Room 3"])
        self.rooms["Room 2"].add_exit("s", self.rooms["Room 3"])
        self.rooms["Room 3"].add_exit("s", self.rooms["Room 2"])

        # List of things we can find in each rooms
        self.items = [
        Light("light", "Small hand light. That can give you much easier access to many places."),
        Map("map", "Map can help you to navigate."),
        Food("food", "You need food to survive"),
        ]

        # Current room is number 1
        self.current_room = self.rooms["Room 1"]

        # Create empty inventory
        self.inventory = []

    def display_room(self):
        # Description of current room
        print(self.current_room.description)
        print("Options:")

        # Show the list of options
        for option in self.current_room.options:
            if option == "n":
                print("  Go to north (n)")
            elif option == "s":
                print("  Go to south (s)")
            elif option == "e":
                print("  Go to east (e)")
            elif option == "w":
                print("  Go to north west (w)")
            elif option == "t":
                print("  Take subject (t)")
            elif option == "i":
                print("  Ignore subject (i)")
            elif option == "p":
                print("  Check inventory (p)")
            elif option == "u":
                print("  Use subject (u)")
            elif option == "d":
                print("  Throw subject (d)") 

        # Shows inventory
        if self.inventory:
            print(f"Your inventory: {', '.join(map(str, self.inventory))}")

    def process_choice(self, choice):
        if choice == "n":
            self.current_room = self.rooms["Room 2"]

        elif choice == "s":
            self.current_room = self.rooms["Room 3"]

        elif choice == "e":
            self.current_room = self.rooms["Room 1"]

        elif choice == "w":
            self.current_room = self.rooms["Room 2"]

        elif choice == "t":
            # Random subjects can be picked up in each rooms
            self.inventory.append(random.choice(self.items))

        elif choice == "i":
            # Ignore subject 
            pass

        elif choice == "p":
            # Inventory check
            if not self.inventory:
                print("Your inventory is empty.")
            else:
                print("Your inventory contains:")
                for item in self.inventory:
                    print(" - " + item)

        elif choice == "u":
        # GWhich subjects you want to use
            item_name = input("Which subject you want to use? ")
            if item_name == "food":
                    Food.use(self)
            elif item_name == "map":
                    Map.use(self)
            elif item_name == "light":
                    Light.use(self)
            else:
                    print("You have no " + item_name + ".")
            for item in self.inventory:
                            if item.name == item_name:
                                self.inventory.remove(item)
        elif choice == "d":
            # Player wants to throw the things not needed
            item_name = input("Which kind of subjects you want to throw out? ")
            for item in self.inventory:
                if item.name == item_name:
                    self.inventory.remove(item)
                    print("You throw " + item.name + ".")
            else:
                print("You dont have " + item.name + ".")

        else:
            # Nothing happen when player put invalid commands. 
            pass

# Game
game = Game()
while True:
  game.display_room()
  choice = input("What would you like to do? ")
  game.process_choice(choice)